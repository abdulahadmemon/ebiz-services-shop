//***** Modules goes here *****//
const express = require('express');
const Joi = require('joi');
const MessageData = require('./schema');
const { UserData } = require('../userModel/schema');
const DealData = require('../dealsModel/schema');
//***** ///// *****//

//***** Express Router to export in module *****//
const app = express();
//***** ///// *****//

//***** Post Request for Login *****//
app.post('/', (req, res)=> {
    const { error } = validateMessageData(req.body);
    if(error) {
        var errors = {
            success:false,
            msg:error.name,
            data:error.details[0].message
        };
        res.send(errors);
        return;
    }
    
    checkMessageIfExist(req.body).then((response)=> {
        var success = {
            success:true,
            msg:'',
            data:response
        };
        res.send(success);
    });
});
//***** ///// *****//

//***** User login data validation function *****//
function validateMessageData(messageData) {
    const schema = Joi.object().keys({
        userId: Joi.string().required(),
        storeId: Joi.string().required(),
        dealId: Joi.string().required(),
        message:Joi.any()
    });
    return Joi.validate(messageData, schema);
}
//***** ///// *****//

async function checkMessageIfExist(body) {
    // return new Promise((res)=> {
        const messages = await MessageData
        .find()
        .and([{storeId: body.storeId, dealId: body.dealId, userId: body.userId}])
        .sort({createdDate: -1});
        if(messages.length > 0) {
            const store = await UserData.findById(messages[0].storeId);
            const deal = await DealData.findById(messages[0].dealId);
            for (var i = 0; i < messages[0].message.length; i++) {
                console.log(messages[0].message[i]);
                if (messages[0].message[i].type === 'user') {
                    if (messages[0].message[i].status === 1 || !messages[0].message[i].status) {
                        messages[0].message[i].status = 2;
    
                        // console.log(result);
                    }
                    else {
                        console.log("no unread");
                    }
                }
                else {
                    console.log("no unread");
                }
            }
            const result = await MessageData.updateMany(
                { _id: messages[0]._id },
                { $set: { message:messages[0].message }}
    
            );
            messages[0].storeName = store.companyName;
            messages[0].dealImage = deal.image;
            messages[0].dealName = deal.name;
            messages[0].dealDate = deal.createdDate;
            
            return (messages[0]);
        }
        else {
            const store = await UserData.findById(body.storeId);
            const deal = await DealData.findById(body.dealId);
            
            const tmp = {
                storeName: store.companyName,
                dealImage: deal.image,
                dealName: deal.name,
                dealDate: deal.createdDate,
                message: []
            };
            
            return (tmp);
        }
        // setTimeout(()=> {
            // return messages;
        // },200);
        
    // });
}

module.exports = app;