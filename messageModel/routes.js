//***** Modules goes here *****//
const express = require('express');
//***** ///// *****//

//***** Express Router to export in module *****//
const app = express.Router();
//***** ///// *****//

//***** Distributing requests *****//

//~~ Check new message ~~//
const checkMessageThread = require('./checkMessage');
app.use('/check', checkMessageThread);

//~~ Post new message ~~//
const postNewMessage = require('./postMessage');
app.use('/chat', postNewMessage);

//~~ Get All message list~~//
const listMessage = require('./getAllList');
app.use('/getList', listMessage);


//~~ Get Image of single deal ~~//
const dealImage = require('./getImages');
app.use('/getImage', dealImage);

module.exports = app;