//***** Modules goes here *****//
const express = require('express');
const Joi = require('joi');
const MessageData = require('./schema');
// const UserData = require('../userModel/schema');
const DealsData = require('../dealsModel/schema');
const consumerData = require('../consumerModel/schema');
const auth = require('../middleware/auth');
//***** ///// *****//

//***** Import Firebase-Admin  *****//
const admin = require("firebase-admin");
//***** ///// *****//

//***** Express Router to export in module *****//
const app = express();
//***** ///// *****//

//***** Post Request for Login *****//
app.post('/', auth, (req, res)=> {
    req.body = JSON.parse(Object.keys(req.body)[0]);

    console.log('request: ' ,req.body);
    const { error } = validateMessageData(req.body);
    if(error) {
        var errors = {
            success:false,
            msg:error.name,
            data:error.details[0].message
        };
        res.send(errors);
        return;
    }
    
    checkMessageIfExist(req.body).then((response)=> {
        console.log('level 1');
        if(response.length != 0)
            postMessageIfExist(response[0]._id, req.body).then((result)=> {
                console.log(result);
                if(result == 2 || result == 3) {
                    var errors = {
                        success:false,
                        msg:'There was an error posting this message.', 
                        data:{}
                    };
                    res.send(errors);
                }
                else {
                    checkMessageIfExist(req.body).then((stores)=> {
                        var success = {
                            success:true,
                            msg:'Message posted successfully.', 
                            data:stores[0]
                        };
                        sendPushNoti(stores[0].userId, stores[0].dealId, req.body.message);
                        res.send(success);
                    });
                    
                }
            });

        else
            postMessageIfNotExist(req.body).then((result)=> {
                if(result == 500) {
                    var errors = {
                        success:false,
                        msg:'There was an error posting this message.', 
                        data:{}
                    };
                    res.send(errors);
                }
                else {
                    var success = {
                        success:true,
                        msg:'Message posted successfully.', 
                        data:result
                    };
                    sendPushNoti(result.userId, result.dealId, req.body.message);
                    res.send(success);
                }
            });
    });
});
//***** ///// *****//

//***** User login data validation function *****//
function validateMessageData(messageData) {
    const schema = Joi.object().keys({
        userId: Joi.string().required(),
        storeId: Joi.string().required(),
        dealId: Joi.string().required(),
        message:Joi.any(),
        lastupdate:Joi.any()
    });
    return Joi.validate(messageData, schema);
}
//***** ///// *****//

async function checkMessageIfExist(body) {

    // return new Promise((res)=> {
        const messages = await MessageData
        .find()
        .and([{userId:body.userId, storeId: body.storeId, dealId: body.dealId}]);
        return messages;
    // });
}

async function postMessageIfExist(_id, body) {
    console.log(body);
    body.message.status= 1;
    const result = await MessageData.updateMany(
        { _id: _id },
        { $push: { message: body.message } , $set :{lastupdate:body.message.time} }
    );
   console.log(_id, result); 
    if(result.ok == 1 && result.nModified == 1) 
        return 1; // 1 = store added
    else if(result.ok == 1 && result.nModified == 0)
        return 2; // 2 = store already subscibed
    else
        return 3;
}

function postMessageIfNotExist(body) {
    return new Promise((res)=> {
        const newMessage = new MessageData(body);
        const save = newMessage.save(function(err) {
            if (err) {return res(500);}
            else {return res(newMessage);}
        });
    });
}

async function sendPushNoti(storeId, dealId, message) {
    console.log('===sendPushNoti===');
    
    const user = await consumerData
    .findById(storeId);
    const deal = await DealsData
    .findById(dealId);

    var registrationToken = [user.gcm_id];
    var options = {
        priority: "high",
        timeToLive: 60 * 60,
        contentAvailable: true,
        sound:'default'
    };
    var payload = {
        notification: {
            title: 'Shopkeeper replied to your message for "'+deal.name+'"',
            body: message.text,
        },
        data: {
            coldstart: 'false',
            dismissed: 'false',
            foreground: 'false',
            type:'new message',
            storeId:storeId,
            dealId:dealId
        }
    };
    console.log('noti: ', payload);
    admin.messaging().sendToDevice(registrationToken, payload, options)
    .then(
        (response)=> {
            console.log("Successfully sent message:", response);
        }
    )
    .catch(
        (error)=> {
            console.log("Error sending message:", error);
        }
    );
}
module.exports = app;