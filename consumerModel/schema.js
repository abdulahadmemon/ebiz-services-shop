const mongoose = require('mongoose');

//***** User Document Schema *****//
const userSchema = new mongoose.Schema({
    fullName: {
        type: String,
        required: true
    },
    userName: {
        type: String,
        required: true
    },
    email: {
        type: String,
        unique: true,
        required: true
    },
    mobile: {
        type: Number,
        required: true
    },
    password: {
        type: String,
        required: true,
        select:false
    },
    gcm_id: String,
    platform: String,
    createdDate:{ type:Date, default:Date.now },
    liked_stores: Array
});

const UserData = mongoose.model('users', userSchema);
//***** ///// *****//

module.exports = UserData;