const path = require('path');
const qrPath = path.join(__dirname,'public/qrCodes');
const dealsImagePath = path.join(__dirname,'public/deals');

const publicDir = {
    qrCodePath: qrPath,
    dealsImagePath: dealsImagePath
};

module.exports = publicDir;