//***** Modules goes here *****//
const express = require('express');
const Joi = require('joi');
const DealData = require('./schema');
const auth = require('../middleware/auth');
const publicDir = require('../publicDir'); //publicDir.dealsImagePath
const fs = require('fs-extra');
var base64Img = require('base64-img');
//***** ///// *****//

//***** Import Firebase-Admin  *****//
const admin = require("firebase-admin");
//const serviceAccount = require("../config/smartlybiz-firebase-adminsdk.json");
//***** ///// *****//

//***** Express Router to export in module *****//
const app = express();
//***** ///// *****//

//***** Post Request for Upload new Deal *****//
app.post('/', auth, async(req, res)=> {
    try {
        const {error} = await validateUserData(req.body);
        if (!error) {
            //***** Delete Deal *****//
            const result = await deleteDeal(req.body);
            if(result == 200) {
                var success = {
                    success: true,
                    msg: 'Deal updated successfuly',
                    data: ''
                }
                res.send(success);
            }

            else if(result == 400) {
                var success = {
                    success: false,
                    msg:'Deal not found',
                    data:'This deal migth be deleted earlier.'
                }
                res.send(success);
            }
        }
    }
    catch(ex) {
        console.log(ex.name);
        if(ex.name == 'ValidationError') {
            var errors = {
                success:false,
                msg:ex.name,
                data:ex.details[0].message
            };
            res.send(errors);
        }
        if(ex.name == 'CastError') {
            var errors = {
                success:false,
                msg:'Deal not found',
                data:'Kindly tryagain later.'
            };
            res.send(errors);
        }
        if(ex.name == 'ReferenceError') {
            var success = {
                success: true,
                msg: 'Deal updated successfuly',
                data: ''
            }
            res.send(success);
        }
    }
});
//***** ///// *****//

//***** Deal data validation function *****//
async function validateUserData(dealObj) {
    const schema = Joi.object().keys({
        _id: Joi.string().required(),
        name: Joi.string().required(),
        description: Joi.string(),
        startDate: Joi.string().required(),
        endDate: Joi.string().required(),
        image: Joi.string(),
        storeId: Joi.string().required(),
    });
    return Joi.validate(dealObj, schema);
}
//***** ///// *****//

//***** Initialing and saving data *****//
async function deleteDeal(dealData) {
    console.log(dealData);
    const deal = await DealData.findByIdAndUpdate(dealData._id, {
            $set:dealData
        });
    console.log(deal);
    
    if(deal) {
        // deleteDealImage(deal.image);
        return 200;
    }
    else
        return 400;
}
//***** ///// *****//
function deleteDealImage(path) {
    const npath = publicDir.dealsImagePath.replace('deals','');
    console.log(npath+path)
    fs.remove(npath+path)
    .then(() => {
        console.log('fs: success!');
    })
    .catch(err => {
        console.error('err: '+ err)
    });
}
module.exports = app;