//***** Modules goes here *****//
const express = require('express');
//***** ///// *****//

//***** Express Router to export in module *****//
const app = express.Router();
//***** ///// *****//

//***** Distributing requests *****//

//~~ Upload ~~//
const uploadModule = require('./upload');
app.use('/upload', uploadModule);

//~~ GetAllDeals ~~//
const getAllDealsModule = require('./allDeals');
app.use('/getAll', getAllDealsModule);

//~~ DeleteDeal ~~//
const DeleteDealModule = require('./deleteDeal');
app.use('/delete', DeleteDealModule);

//~~ EditDeal ~~//
const EditDealModule = require('./edit');
app.use('/edit', EditDealModule);

module.exports = app;