//***** Modules goes here *****//
const express = require('express');
const Joi = require('joi');
const publicDir = require('../publicDir'); //publicDir.dealsImagePath
const UploadDealSchema = require('./schema');
const {UserData} = require('../userModel/schema');
const ConsumerSchema = require('../consumerModel/schema');
var base64Img = require('base64-img');
const auth = require('../middleware/auth');
//***** ///// *****//

//***** Import Firebase-Admin  *****//
const admin = require("firebase-admin");
//const serviceAccount = require("../config/smartlybiz-firebase-adminsdk.json");
//***** ///// *****//

//***** Express Router to export in module *****//
const app = express();
//***** ///// *****//

//***** Post Request for Upload new Deal *****//
app.post('/', auth, async(req, res)=> {
    const {error} = await validateUserData(req.body);
    if(error) {
        var errors = {
            success:false,
            msg:error.name, 
            data:error.details[0].message
        };
        res.send(errors);
        return;
    }

    //***** Save Deal *****//
    const result = await createDeal(req.body);
    res.send(result);
    if(result.status == 200) {
        sendPushNoti(result.data.storeId, result.data._id, req.body.description);
    }
});
//***** ///// *****//

//***** Deal data validation function *****//
async function validateUserData(uploadObj) {
    const schema = Joi.object().keys({
        name: Joi.string().required(),
        description: Joi.string(),
        startDate: Joi.string().required(),
        endDate: Joi.string().required(),
        image: Joi.string(),
        storeId: Joi.string().required()
    });
    return Joi.validate(uploadObj, schema);
}
//***** ///// *****//

//***** Initialing and saving data *****//
async function createDeal(dealsData) {
    return new Promise((res)=> {
        base64Img.img(dealsData.image, publicDir.dealsImagePath, dealsData.name+'-'+Date.now(), async function(err, filepath) {
            if (err) {
                var errors = {
                    success: false,
                    status:500,
                    msg:err,
                    data:{}
                };
                return res(errors);
            };

            const store = await UserData.findById(dealsData.storeId);
            console.log('credits: '+store.credits);
            if(store.credits > 2) {
                console.log('if');
                const n = filepath.indexOf('deals');
                const result = filepath.substring(n);
                dealsData.image = result;
                
                store.credits = store.credits - 2;
                const updateRewardPoints = new UserData(store);
                const saveRewardPoints = updateRewardPoints.save();

                const deal = new UploadDealSchema(dealsData);
                const save = await deal.save();
                var success = {
                    success: true,
                    status:200,
                    msg:'Deal saved successfully', 
                    data:save
                };
                console.log(success);
                return res(success);
            }

            else {
                console.log('else');
                var success = {
                    success: true,
                    status:401,
                    msg:'Deal cannot be posted.', 
                    data:'Insufficient credits.'
                };
                console.log(success);
                return res(success);
            }
        });
    });
}
//***** ///// *****//

async function sendPushNoti(storeId, dealId, description) {
    const store = await UserData
    .findById(storeId);
    
    const users = await ConsumerSchema
    .find()
    .and( [ {_id:store.liked_by} ] );

    var recievers = [];
    for(var i = 0; i < users.length; i++) {
        recievers.push(users[i].gcm_id);
        if(users.length == (i+1)) {
            var registrationToken = recievers;
            console.log(registrationToken);
            var options = {
                priority: "high",
                timeToLive: 60 * 60,
                contentAvailable: true,

            };
            var payload = {
                notification: {
                  title: store.companyName+" posted a new deal.",
                  body: description,
                  sound:'default'
                },
                data: {
                  type:'new deal',
                  storeId:storeId,
                  dealId: dealId.toString()
                }
            };
            console.log('noti: ', payload);
            admin.messaging().sendToDevice(registrationToken, payload, options)
            .then(
                (response)=> {
                    console.log("Successfully sent message:", response);
                }
            )
            .catch(
                (error)=> {
                    console.log("Error sending message:", error);
                }
            );
        }
    }

}
module.exports = app;