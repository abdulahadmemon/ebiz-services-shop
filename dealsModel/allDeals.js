//***** Modules goes here *****//
const express = require('express');
const Joi = require('joi');
const DealsData = require('./schema');
//***** ///// *****//

//***** Express Router to export in module *****//
const app = express.Router();
//***** ///// *****//

//***** Post Request for Login *****//
app.post('/', (req, res)=> {
    const { error } = validateData(req.body);
    if(error) {
        var errors = {
            success:false,
            msg:error.name, 
            data:error.details[0].message
        };
        res.send(errors);
        return;
    }
    
    checkDeal(req.body).then((response)=> {
        // console.log(response);
        if(!response[0]) {
            var errors = {
                success:false,
                msg:'No Deals Found', 
                data:{}
            };
            res.send(errors);
        }
        else {
            var success = {
                success:true,
                msg:'Deals Found', 
                data:response
            };
            res.send(success);
        }
    });
});
//***** ///// *****//

//***** User login data validation function *****//
function validateData(storeId) {
    // console.log(storeId);
    const schema = Joi.object().keys({
        storeId: Joi.string().required()
    });
    return Joi.validate(storeId, schema);
}
//***** ///// *****//

async function checkDeal(body) {
    return new Promise((resolve)=> {
        const deals = DealsData
        .find()
        .and([{storeId:body.storeId}])
        .sort({createdDate: -1});
        return resolve(deals);
    }); 
}

module.exports = app;