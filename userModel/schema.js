const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');
const config = require('config');

//***** User Document Schema *****//
const userSchema = new mongoose.Schema({
    fullName: {
        type: String,
        min: 4,
        max: 30,
        required: true
    },
    userName: {
        type: String,
        min: 4,
        max: 30,
        required: true
    },
    email: {
        type: String,
        unique: true,
        required: true
    },
    mobile: {
        type: Number,
        required: true
    },
    password: {
        type: String,
        required: true,
        // select:false
    },
    companyName: {
        type: String,
        max: 50,
        required: true
    },
    credits: {
        type: Number,
        required: true
    },
    websiteUrl: String,
    facebookUrl: String,
    qrcodePath: String,
    is_premium: Boolean,
    gcm_id: String,
    platform: String,
    createdDate:{ type:Date, default:Date.now },
    liked_by:Array
});

function generateAuthToken() {
    const token = jwt.sign({_id: this._id}, config.get('jwtPrivateKey'));
    return token;
}

const UserData = mongoose.model('shopkeepers', userSchema);
//***** ///// *****//

exports.UserData = UserData;
exports.generateAuthToken = generateAuthToken;