//***** Modules goes here *****//
const express = require('express');
const Joi = require('joi');
const {UserData} = require('./schema');
//***** ///// *****//

//***** Express Router to export in module *****//
const app = express();
//***** ///// *****//

//***** Post Request for Login *****//
app.post('/', async(req, res)=> {
    const { error } = validateUserData(req.body);
    if(error) {
        var errors = {
            success:false,
            msg:error.name, 
            data:error.details[0].message
        };
        res.send(errors);
        return;
    }
    
    const user = await getUser(req.body._id);
    if(!user){
        const response = {
            success:false,
            msg:'No user found', 
            data:{}
        }
        res.send(response);
    }
    else {
        const response = {
            success:true,
            msg:'User found.', 
            data:user
        }
        res.send(response);
    }
});
//***** ///// *****//
async function getUser(id) {
    const user = await UserData.findById(id).select('-password');
    return user;
}
//***** User login data validation function *****//
function validateUserData(userData) {
    const schema = Joi.object().keys({
        _id: Joi.string().required()
    });
    return Joi.validate(userData, schema);
}
//***** ///// *****//

module.exports = app;