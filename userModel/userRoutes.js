//***** Modules goes here *****//
const express = require('express');
//***** ///// *****//

//***** Express Router to export in module *****//
const app = express.Router();
//***** ///// *****//

//***** Distributing requests *****//

//~~ Signup ~~//
const signupModule = require('./signup');
app.use('/signup', signupModule);
//***** ///// *****//

//~~ Login ~~//
const loginModule = require('./login');
app.use('/login', loginModule);
//***** ///// *****//

//~~ Edit Profile ~~//
const editModule = require('./editProfile');
app.use('/edit', editModule);
//***** ///// *****//

//~~ Edit Profile ~~//
const updateGcmModule = require('./updateGcm');
app.use('/updategcm', updateGcmModule);
//***** ///// *****//

//~~ Edit Profile ~~//
const getUserModule = require('./getUser');
app.use('/getUser', getUserModule);
//***** ///// *****//

module.exports = app;