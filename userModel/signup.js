//***** Modules goes here *****//
const _ = require('lodash');
const bcrypt = require('bcryptjs');
const express = require('express');
const Joi = require('joi');
var qr = require('qr-image');
const {UserData} = require('./schema');
//***** ///// *****//

//***** Express Router to export in module *****//
const app = express();
//***** ///// *****//

//***** Post Request for Signup *****//
app.post('/', (req, res)=> {
    const { error } = validateUserData(req.body);
    if(error) {
        var errors = {
            success:false,
            msg:error.name,
            data:error.details[0].message
        };
        res.send(errors);
        return;
    }

    checkUser(req.body).then(
        resolve=> {
            console.log('before if', resolve);
            if(resolve == undefined) {
               console.log('resolve if');
                createUser(req.body).then (
                    (response)=> {
                        console.log('createUser:', response);
                        const date = new Date();
                        const correctDate = date.getFullYear()+'_'+date.getMonth()+'_'+date.getDate()+' '+date.getHours()+'-'+date.getMinutes()+'-'+date.getSeconds()+'-'+date.getMilliseconds();
                        const publicDir = require('../publicDir');
                        const filename = req.body.companyName+'_'+correctDate;
                        generateQR(filename);
                        const path = publicDir.qrCodePath+'/'+filename+'.png';
                        if(response) updateQrPath(response._id, path);
                        
                        response.qrcodePath = path;
                        const requestData = {
                            success: true,
                            msg: 'User created successfully.',
                            data: response
                        };
                        res.send(requestData);
                    }
                );  
            }

            else {
                console.log('checkUser else');
                var errors = {
                    success:false,
                    msg:'Duplicate', 
                    data:'Email already exist'
                };
                res.send(errors);
                return;
            }
        }
    );
    return;
});
//***** ///// *****//

//***** User signup data validation function *****//
function validateUserData(userData) {
    // console.log(userData);
    const schema = Joi.object().keys({
        fullName: Joi.string().min(4).max(30).required(),
        userName: Joi.string().min(4).max(30).required(),
        email: Joi.string().email({ minDomainAtoms: 2 }).required(),
        mobile: Joi.number().required(),
        password: Joi.string().min(5).regex(/^[a-zA-Z0-9]{3,30}$/),
        companyName: Joi.string().max(50),
        websiteUrl: Joi.string().allow('').optional().regex(/^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$/),
        facebookUrl: Joi.string().allow('').optional().regex(/^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$/),
        qrcodePath: Joi.string(),
        is_premium: Joi.boolean().required(),
        gcm_id: Joi.string(),
        platform: Joi.string(),
        credits: Joi.number().max(50)
    });
    return Joi.validate(userData, schema);
}
//***** ///// *****//

//***** Initialing and saving data *****//
async function createUser(userData) {
    
    const user = new UserData(userData);
    const salt = await bcrypt.genSalt(10);
    const hashed = await bcrypt.hash(user.password, salt);
    user.password = hashed;

    const result = await user.save();
    return result;
}
//***** ///// *****//

//***** Generating and saving Qr-Image *****//
function generateQR(data) {
    var qr_png = qr.image(data, {type: 'png' });
    qr_png.pipe(require('fs').createWriteStream('public/qrCodes/'+data+'.png'));
}
//***** ///// *****//

//***** Find User and return function *****//
async function checkUser(body) {
    console.log("email:"+body.email, "password:"+body.password);
    const user = await UserData
    .find()
    .and([{email:body.email}]);
    console.log('USER:');
    console.log(user);
    return (user[0]);
}
//***** ///// *****//

//***** Find User and return function *****//
async function updateQrPath(_id, name) {
    const user = await UserData.findById(_id);
    user.qrcodePath = name;
    const result = await user.save();
}
//***** ///// *****//
module.exports = app;