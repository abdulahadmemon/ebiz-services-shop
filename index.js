//***** Modules goes here *****//
const config = require('config');
// const debug_N = require('debug')('app:startup');
// const debug_DB = require('debug')('app:db');
const express = require('express');
//***** ///// *****//

if(!config.get('jwtPrivateKey')){
  console.error('FATAL ERROR: jwtPrivateKey is not defined');
  process.exit(1);
}
//***** Import & Initialize Firebase-Admin  *****//
const admin = require("firebase-admin");
const serviceAccount = require("./config/smartlybiz-firebase-adminsdk.json");
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://smartlybiz.firebaseio.com"
});
//***** ///// *****//

//***** Initialize express *****//
const app = express();
//***** ///// *****//

//***** Static file provider *****//
var publicDir = require('path').join(__dirname,'/public');
app.use(express.static(publicDir));
//***** ///// *****//

//***** Route all requests to route.js *****//
const routesModule = require('./routes');
app.use('/api', routesModule);
//***** ///// *****//

// app.set('view engine', 'pug');
// app.set('views', './views'); //default


//***** Assigning Port *****//
app.listen(process.env.PORT, ()=>console.log('Listening to port '+ process.env.PORT));
// app.listen(4200);
//***** ///// *****//